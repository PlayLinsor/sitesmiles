<?php
	session_start();
	$StatusText = "Страница массового импорта";
	include "function/core.php";

	if ($_POST['btn']!=null) {
		$category = "";
		if ($_POST['categors']==null) $category=htmlspecialchars($_POST['category']);
			else $category=htmlspecialchars($_POST['categors']);
		
		if ($DefaultPassword!=$_POST['pass']){
			$StatusText="Не верно набран пароль";
			$StatusIntf="label label-warning";
		} else {
			$Links = $_POST["links"];
			$ArrayLink = explode(',', $Links);
			for ($i = 0; $i < count($ArrayLink); $i++) {
				if (trim($ArrayLink[$i])=='') continue;
				$tempFileName = $temp.genTempName().".png";
				
				echo Copy_Curl(trim($ArrayLink[$i]),$tempFileName);
				
				if (file_exists($tempFileName)) {
					$StatusText = SaveSmail($category, $tempFileName,1);
				} else {
					$StatusText="Произошел сбой при загрузке ".$ArrayLink[$i];
					$StatusIntf="label label-error";
				}
			}
			unlink($tempFileName);
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<title>Наборы смайликов</title>
<link href="styles/bootstrap.css" rel="stylesheet" type="text/css">
<link href="styles/style.css" rel="stylesheet" type="text/css">
</head>
<body>
  <?php ShowError() ?>
<div class="container-fluid">
<div class="row-fluid">
  <div class="span4"></div>
  
  <div class="span4">
  <div class="addPanel">
  	<div align="center"><p align="center" class="<?php print $StatusIntf ?>" ><?php print $StatusText?></p></div>
  	<form action="import.php" enctype="multipart/form-data" method="POST">
  		<select name="category">
  			<?php echo AllCategories()?>
		</select>
		<input type="text" placeholder="Поле для создания новой категории" name="categors" id="sptext" >
  		<input type="password" placeholder="Пароль от спамеров" name="pass" required="required" id="sptext">
  		
  		<textarea name="links" rows="10" placeholder="ссылки на картинки разделенные запятыми." style="width: 92%"></textarea>
  		
  		<input type="submit" value="Залить смайлики" name="btn">
  	</form>
  </div>
  <div align="center"><a href="index.php">Возрат на главную</a></div>
  </div>
  <div class="span4"></div>
   
       
   </div>
 </div>

</body>
</html>