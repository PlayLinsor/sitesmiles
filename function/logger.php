<?php
/////////////////////////////////////////////////////////////
//		Файл ведения логов события 1.0 
//				   
//  		Create by PlayLinsor
//				  24.06.14
/////////////////////////////////////////////////////////////

/**
 * Позволяет вести Log файл событий
 * @author PlayLinsor
 */
class logger{
	private  static $FileName='Debug.html';
	private  static $FileHandle = null;

	// Иницилизация файла логгирования
	private  static function Init(){
	if (self::$FileHandle==null){
		if (!file_exists(self::$FileName)) {
			// Если файла нет то создаем заготовку нового файла

			self::$FileHandle = fopen(self::$FileName,'a');
			
			self::WriteData(self::getHeaderData());
			
		} else {
			// если файл есть то просто его открываем
			
			self::$FileHandle = fopen(self::$FileName,'a');
			self::WriteData("<hr>");
			
		}
		
		if (!self::$FileHandle) echo "Беда с файлом Логгирования";
		
		}
	}
	
	// преобразование в ячейку
	private static function log($varible,$Sector='Note',$class=' '){
			$Data = "<div class='$class'>";
			$Data .= "[ ".date("d-m-Y H:i:s")." ]";
			$Data .="[".$Sector."]";
			$Data .= htmlspecialchars($varible)."</div>";
			
			self::WriteData($Data);
		}
		
	/** Записывает в файл логгирования массив в виде текста
	 * @param string $varible - Массив
	 * @param string $Sector - категория
	 */
	public static function VarArray($varible,$Sector=" "){
		foreach ($varible as $key=>$value){
			if ($Sector!=" ") $key = $Sector."/".$key; 
			if (is_array($value)) {
				self::VarArray($value,$key);
			} else {
				self::log($value,$key,'array');
			}
		}
	}
		
	/** Записывает в файл логгирования обычный текст
	 * @param string $Text - основной текст
	 * @param string $Sector - категория
	 */
	public static function text($Text,$Sector="Заметка"){
		self::log($Text,$Sector," ");
	}
	
	/** Записывает в файл логгирования текст и выделяет его красным цветом
	 * @param string $Text - основной текст
	 * @param string $Sector - категория
	 */
	public static function error($Text,$Sector="Ошибка"){
		self::log($Text,$Sector,"alert alert-danger");
	}
	
	/** Записывает в файл логгирования текст и выделяет его желтым цветом
	 * @param string $Text - основной текст
	 * @param string $Sector - категория
	 */
	public static function warning($Text,$Sector="Предупреждение"){
		self::log($Text,$Sector,"alert alert-warning");
	}
	
	/** Записывает в файл логгирования текст и выделяет его синим цветом
	 * @param string $Text - основной текст
	 * @param string $Sector - категория
	 */
	public static function info($Text,$Sector="Внимание"){
		self::log($Text,$Sector,"alert alert-info");
	}
	
	/** Записывает в файл логгирования текст и выделяет его зеленым цветом
	 * @param string $Text - основной текст
	 * @param string $Sector - категория
	 */
	public static function success($Text,$Sector="Успех"){
		self::log($Text,$Sector,"alert alert-success");
	}
	
	/** Указывает файл логгирования
	 * @param string $FileName - путь
	 */
	public static function LogFile($FileName){
		self::$FileName = $FileName;
	}
	
	// непосредственная запись в файл логгирования
	private static function WriteData($Date){
		if (self::$FileHandle==null) self::Init();

			fwrite(self::$FileHandle,$Date);
	} 
	
	// Функция - ресурс. Создаёт начало файла логгирования если его не было до этого.
	private static function getHeaderData(){
		$Header = "<html><head><title>Файл Логирования от - ".date("d-m-Y H:i:s");
		$Header .= '</title><meta http-equiv="Content-Type" content="text/html; charset=utf8" />';
		$Header .="<style>
					.alert-info{
					background-color:#d9edf7;
					border-color:#bce8f1;
					color:#31708f;}
			
					.alert-warning{
					background-color:#fcf8e3;
					border-color:#faebcc;
					color:#8a6d3b;}
			
					.alert-danger{
					background-color:#f2dede;
					border-color:#ebccd1;
					color:#a94442;}
					
					.alert-success{
					background-color:#dff0d8;
					border-color:#d6e9c6;
					color:##3c763d;}
					
					.array{
					font-size:12px;
					background-color:#ddd;}
				
					.alert{
						border: 1px solid transparent;
					}
					</style></head><body>";
		return $Header;
	}
}

?>