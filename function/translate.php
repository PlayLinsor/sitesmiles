<?php
///////////////////////////////////////////////////////////////////////
// Транскрипция русских надписей в латинский вариант
// 
// v 1.0.0.1
//
// Cовместима для хранения файлов в ФС: Windows, Unix
// Совместима БД SQLite 3.0
//////////////////////////////////////////////////////////////////////
function getTranslateBase(){
	$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',
			'ё' => 'e\'', 'ж' => 'zh',  'з' => 'z',
			'и' => 'i',   'й' => 'iy',  'к' => 'k',
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'h',   'ц' => 'c',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
			'ь' => '~',   'ы' => 'y',   'ъ' => '~',
			'э' => 'ye',  'ю' => 'yu',  'я' => 'ya',
			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
			'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
			'И' => 'I',   'Й' => 'Iy',  'К' => 'K',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
		    'Ы' => 'Y',   
			'Э' => 'Ye',   'Ю' => 'Yu',  'Я' => 'Ya',
	);
	
	return $converter;
}


function str2url($str) {

	// переводим в транслит
	$str = strtr($str, getTranslateBase());
	
	// заменям все ненужное нам на "_"
	$str = preg_replace('~[^-A-zA-я0-9_\']+~u', '_', $str);
	
	// удаляем начальные и конечные '-'
	$str = trim($str, "_");

	return $str;
}

function url2rus($str){
	
	// переводим в транслит
	$str = strtr($str, array_flip(getTranslateBase()));
	
	// заменям все ненужное нам на " "
	$str = preg_replace('~[^-А-я0-9\'ёЁ]+~u', ' ', $str);
	
	// удаляем начальные и конечные ' '
	$str = trim($str, " ");
	
	return $str;
	
}
