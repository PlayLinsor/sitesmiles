<?php
/////////////////////////////////////////////////////////////
// Модуль для работы с БД SqlLite 3.
// Develop by smail.playlinsor.ru
//
// Creaty by PlayLinsor 23.06.14
/////////////////////////////////////////////////////////////

class DataBase {
	private static $ClassHandler = null;
	private $DB_CONNECT;
	private $PathDB = 'function/DB/db.sqlite3';
	private $ErrorText = '';
	
	private function __construct(){	
		try {
			if (!file_exists($this->PathDB)) {
				// Если нет файла Бд то создаём его и запихиваем первоначальную таблицу
					$this->DB_CONNECT = new PDO('sqlite:'.$this->PathDB);
					self::Query('PRAGMA encoding = "UTF-8"');
					self::CheckAndEchoError();
					$this->CreateStartBD();
			} else {
				// если файл с Бд Найден.
				$this->DB_CONNECT = new PDO('sqlite:'.$this->PathDB);
				self::Query('PRAGMA encoding = "UTF-8"');
				self::CheckAndEchoError();
				
			}
		} catch (PDOException $e){
			$this->SaveError($e->errorInfo); 
		}
	}
	
	// Инициализация работы с Бд. 
	public static function getInstans(){
		if (self::$ClassHandler == null)
			self::$ClassHandler = new self();
		return self::$ClassHandler;
	}

	// прямой запрос
	public function Query($query){
		Try {
		$result = $this->DB_CONNECT->query($query);
		self::CheckAndEchoError();
		return $result;
		} catch (PDOException $e){
			echo $e;
		}
	}
	
	public function QueryReturnArray($query){
		$Ar = self::Query($query);
		if ($Ar) $results = $Ar->fetchAll();
			else {
				$results = array();
			}
		return $results;

	}
	// Проверяем наявность ошибок
	public function CheckAndEchoError(){
		$error = $this->DB_CONNECT->errorInfo();
		if ($error[0]!='0'){
			$this->SaveError("Ошибка базы данных : ".$error[2]."  ( ".$error[1]." )")	;
		}
	}
	
	public function CreateStartBD(){
		$query = 'CREATE TABLE IF NOT EXISTS main (
					Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
					FileName VARCHAR(100) NOT NULL ,
					Category varchar(100) NOT NULL)';
		self::Query($query);
	}
	
	public  function GetErrorLine(){
		return $this->ErrorText;
	}
	
	private function SaveError($Error){
		$this->ErrorText .= $Error."\n";
	}
}

//$DB = DataBase::getInstans();
