<?php
//////////////////////////////////////////////////
// Сайт со смайликами
// v 1.0
// Дата разработки 19.06.14
//
///////////  Подключаемые функции  ///////////////

require_once 'translate.php';
require_once 'DataBase.php';
require_once 'config.php';

//////////////////////////////////////////////////

// Возвращает все элементы массива
function GetAllFiles() {
	$tempArray = array();
	
	$DB = DataBase::getInstans();
	$query = "SELECT Category,FileName FROM main";
	
	$array  = $DB->QueryReturnArray($query);
	foreach ($array as $value) {
		$tempArray[$value['Category']][] = $value['FileName'];
	}
	
	return $tempArray;

}

function ShowAllCategories(){
	
	foreach ( $_SESSION['AllFileData'] as $key => $value) {
		print BildMenuItem($key, '?category='.$key);
	}
}

function ShowImagesInCategory($key){
	if ($_SESSION['AllFileData'][$key][0]==null) return print("Не найдено элементов");
	
	$ArrayData = $_SESSION['AllFileData'][$key];
	$num=0;
	foreach ($ArrayData as $value){
		print BildImageItem($value,$num++);
	}
}

function BildMenuItem($Name,$Link){
	return '<a href="'.$Link.'" class="btn btn-info">'.$Name.'</a>';
}

function BildImageItem($ImageFile,$classNum){
	$directory = $GLOBALS[directory];
	
	$CopyClass="Cp".$classNum;
	
	$Item = '<div class="image-panel">';
	
	
	if ($_SESSION['ACCESS']=='Admin') {
		$Item .="<div class='admin-menu'>";
		$Item .="<a href='admin.php?type=remove&file=".$ImageFile."'> Remove </a> |";
		$Item .="<a href='admin.php?type=delete&file=".$ImageFile."'> Delete </a>";
		$Item .="</div>";
	}
	
	$Item .= '<img src="'.$directory.$ImageFile.'" class="img-rounded">';

	$Item .='<span class="'.$CopyClass.'">http://'.$_SERVER['HTTP_HOST'].'/'.$directory.$ImageFile.'</span><a href="#" class="CPurl '.$CopyClass.'" >Копировать</a></div>';

	
	return $Item;
}

function ShowError(){
	$DB = DataBase::getInstans();
	$error = $DB->GetErrorLine();

	if ($error=="") return null;
	
	echo '<div class="ErrorOut">'.$error.'</div>';
}
///////////////////////////////////////////// add.php
function AllCategories(){

	$Text = "<option disabled>Выберите категорию</option>";
	foreach ( $_SESSION['AllFileData'] as $key => $value) {
		$Text .= "<option value='".$key."'>".$key."</option>";
	}

	return $Text;
}


function SaveSmail($Category,$DestFile,$TypeCopy=0){

	
	$directory = $GLOBALS[directory];
	
	$_SESSION['AllFileData'] = GetAllFiles();
	
	if ($_SESSION['AllFileData'][$Category]==null) {
		$Number="100" ;
	} else {
		asort($_SESSION['AllFileData'][$Category]);
		$FileNameArray = explode(".",end($_SESSION['AllFileData'][$Category]));
		$Number = substr($FileNameArray[0],strlen($FileNameArray[0])-3,3)+1;
	}
	
	$Category_convert = str2url($Category);
	
	$File = $Category_convert.$Number.".png";
	

	if ($TypeCopy==0) move_uploaded_file($DestFile,$directory.$File);
		else copy($DestFile, $directory.$File);

	
	$DB = DataBase::getInstans();
	
	$Category_convert = trim(htmlspecialchars($Category));
	
	$query = "INSERT INTO main(FileName,Category)VALUES ('$File','$Category') ;";
	
	$DB->Query($query);
	
	return "Успех!";
}

function moveSmail($Category,$DestFile){
	$directory = $GLOBALS[directory];
	
	$_SESSION['AllFileData'] = GetAllFiles();
	
	if ($_SESSION['AllFileData'][$Category]==null) {
		$Number="100" ;
	} else {
		asort($_SESSION['AllFileData'][$Category]);
		$FileNameArray = explode(".",end($_SESSION['AllFileData'][$Category]));
		$Number = substr($FileNameArray[0],strlen($FileNameArray[0])-3,3)+1;
	}
	
	$Category_convert = str2url($Category);
	
	$File = $Category_convert.$Number.".png";
	
	copy($directory.$DestFile,$directory.$File);
	
	$DB = DataBase::getInstans();
	
	$Category_convert = trim(htmlspecialchars($Category));
	$query = "INSERT INTO main(FileName,Category)VALUES ('$File','$Category') ;";
	
	$DB->Query($query);
	$DB->CheckAndEchoError();
	
	return 1;
	}
	
///////////////////////////////////////////// admin.php

function BuidForm($type,$message="Приветик^^",$otherParam=""){
	$directory = $GLOBALS[directory];
	
///////////////////////////////////////////////////////////	
	// вход в режим администрирования
	if ($type=="login"){		
		$Text = 'Вход в режим Администрирования';
		$Text .= '<form action="admin.php?type=enter" enctype="multipart/form-data" method="POST">';
		$Text .= '<input type="password" placeholder="Пароль для адиминистрирования" name="password" required="required" id="sptext">';
		$Text .= '<input type="submit" value="Попытать счастья" name="btn"></form></div>';
		
		return $Text;
	}
	// Выход из режима администрирования
	if ($type=="exit"){
		$_SESSION['ACCESS']='';
		return BuildInfoMessage('Вышли из режима администратора');
	}
	
//////////////////////////////////////////////////////////////	
// Перемещения файла
	if ($type=="remove"){
		
		if (!file_exists($directory.$message)) return BuildInfoMessage("Не найдено файла");
		
		$Text = "<img src=".$directory.$message." class='imageInfo' >";
		$Text .=BuildInfoMessage("Переместить в");
		$Text .='<form action="admin.php?type=remoteconfirm&file='.$message.'" enctype="multipart/form-data" method="POST">';
  		$Text .='<select name="category">'.AllCategories().'</select>';
  		$Text .='<input type="submit" value="переместить" name="btn"></form>';

  		return $Text;
	}
// Перемещения файла 2 стадия
	if ($type=="remoteconfirm"){

		if (file_exists($directory.$message))
		{
			moveSmail($otherParam,$message);
			
			unlink($directory.$message);
			
			$DB = DataBase::getInstans();
			$DB->Query("delete from main WHERE FileName='".htmlentities($message)."';");
			$DB->CheckAndEchoError();
			
			return BuildInfoMessage("Успешно перемещён");
			
		} else return "Не найден файл для перемещения";
	}
// Удаление файла	
	if ($type=="delete"){
		
		if (!file_exists($directory.$message)) return BuildInfoMessage("Не найдено файла");
		
		$Text = "<img src=".$directory.$message." class='imageInfo' >";
		$Text .=BuildInfoMessage("Действительно удалить ?");
		$Text .='<a href="admin.php?type=deleteconfirm&file='.$message.'">Удалить</a>';

		return $Text;
	}
// Удаление файла стадия 2
	if ($type=="deleteconfirm") {
		if (file_exists($directory.$message))
		{
			unlink($directory.$message);
			$DB = DataBase::getInstans();
			$DB->Query("delete from main WHERE FileName='".htmlentities($message)."';");
			$DB->CheckAndEchoError();
			
			return BuildInfoMessage("Успешно удалёно");
		} else return "Не найден файл для удаления";
	}
// Вывод простого сообщения
	if ($type=="message") {
		return BuildInfoMessage($message);
	}
// Вход в режим админа	
	if ($type=="enter"){
		$AdminPassword = $GLOBALS['AdminPassword'];

		if ($message==$AdminPassword) {
			$_SESSION['ACCESS']='Admin';
			return BuildInfoMessage("Добро пожаловать");
		}
		return BuildInfoMessage('Неправильный пароль'); 
	}
	
	return BuildInfoMessage("Доступ запрещен");
}

function BuildInfoMessage($message){
	return '<div class="InfoOut">'.$message.'</div>';
}


/////////////////////////////////////////////////////////// import.php
function genTempName(){
	if ($_SESSION['RandFileName']=='') $_SESSION['RandFileName'] = md5(rand(1, 3000));
	return $_SESSION['RandFileName'];
}



function Copy_Curl($url,$file) // Copy with oficial site in my site
{
	$cUrl = curl_init();
	if ($cUrl==null) DIE("НЕ ПОДКЛЮЧЕН МОДУЛЬ cURL");

	curl_setopt($cUrl, CURLOPT_URL, $url);
	curl_setopt($cUrl, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($cUrl, CURLOPT_HEADER, 0);
	curl_setopt($cUrl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)");
	//curl_setopt($cUrl, CURLOPT_TIMEOUT, $timeout);
	
	curl_setopt($cUrl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($cUrl, CURLOPT_SSL_VERIFYHOST, false);
	
	$content = curl_exec($cUrl);
	if ($content!=null)
	{
		$r=fopen($file,'w');
		fwrite($r,$content);
		fclose($r);
		unset($cUrl);
		return 1;
	} else
	{
		return 0;
	}
}
