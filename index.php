<?php>
	session_start();
	include "function/core.php";
	
	$_SESSION['AllFileData'] = GetAllFiles();

	if ($_GET['category']!=null) $key=$_GET['category'];
		else $key="0";
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<title>Наборы смайликов</title>
<link href="styles/bootstrap.css" rel="stylesheet" type="text/css">
<link href="styles/style.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="scripts/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="scripts/jquery.zclip.js"></script>
<script type="text/javascript" src="scripts/CopyFunction.js"></script>

</head>
<body>
<div class="container-fluid">
<h2>Наборы смайликов \(^_^)/</h2>
  <?php ShowError() ?>
  <?php If($_SESSION['ACCESS']=='Admin') echo "<div class='InfoOut'>Режим администратора<a href='admin.php?type=exit'> (Выход)</a></div>"?>
 <div class="row-fluid">
  <div class="span3 left-sidebar">
  
   <a href="add.php" class="btn btn-success">Добавить смайлик</a>
   <div class="text-separator">Наборы</div>
   		  <!-- BildMenuMenuItem -->
   		  <?php ShowAllCategories() ?>
  </div>
  
  <div class="span9 right-sidebar">
        <?php 
       
        if ($key=="0") echo "<p align='center'>Привет! Я Рад тебя видеть, надеюсь этот сайт немного поможет, Удачи))</p>";
       	 else ShowImagesInCategory($key) ?>
       
   </div>
 </div>
</div>

<footer>
Create by Playlinsor, special for Dasha(WannaSleep)
<br>
<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=26070243&amp;from=informer"
target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/26070243/3_1_FFFFFFFF_FFFFFFFF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:26070243,lang:'ru'});return false}catch(e){}"/></a>
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter26070243 = new Ya.Metrika({id:26070243,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/26070243" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</footer>

</body>
</html>