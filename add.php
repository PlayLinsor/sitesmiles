<?php
	session_start();
	$StatusText = "Думаю, обьяснять не надо))";
	include "function/core.php";

	if ($_POST['btn']!=null) {
		$category = "";
		if ($_POST['categors']==null) $category=htmlspecialchars($_POST['category']);
			else $category=htmlspecialchars($_POST['categors']);
		
		if ($DefaultPassword!=$_POST['pass']){
			$StatusText="Не верно набран пароль";
			$StatusIntf="label label-warning";
		} else {
			$exp = explode('/',$_FILES['smile']['type']);
			if ($exp[0]!='image') {
				$StatusText="Файл не являются изображением";
				$StatusIntf="label label-warning";
			} else {
				if ($_FILES['smile']['error']!=0){
					$StatusText="Ошибка сервера № ошибки:".$_FILES['smile']['error'];
					$StatusIntf="label label-error";
				} else {
					$StatusText = SaveSmail($category, $_FILES['smile']['tmp_name']);
				}
			}
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<title>Наборы смайликов</title>
<link href="styles/bootstrap.css" rel="stylesheet" type="text/css">
<link href="styles/style.css" rel="stylesheet" type="text/css">
</head>
<body>
  <?php ShowError() ?>
<div class="container-fluid">
<div class="row-fluid">
  <div class="span4"></div>
  
  <div class="span4">
  <div class="addPanel">
  	<div align="center"><p align="center" class="<?php print $StatusIntf ?>" ><?php print $StatusText?></p></div>
  	<form action="add.php" enctype="multipart/form-data" method="POST">
  		<select name="category">
  			<?php echo AllCategories()?>
		</select>
		<input type="text" placeholder="Поле для создания новой категории" name="categors" id="sptext" >
  		<input type="password" placeholder="Пароль от спамеров" name="pass" required="required" id="sptext">
  		<input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
  		<input type="file" name="smile" required="required">
  		<input type="submit" value="Залить смайлик" name="btn">
  	</form>
  </div>
  <div align="center"><a href="index.php">Возрат на главную</a></div>
  </div>
  <div class="span4"></div>
   
       
   </div>
 </div>

</body>
</html>